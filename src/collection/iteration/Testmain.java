package collection.iteration;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Testmain {

	public static void main(String[] args) {
		
		
		System.out.println(new Date());
		
		/*
		 * List<String> list = new ArrayList<String>(); list.add("test");
		 * list.add("saudagar"); list.add("anil"); list.add("yadav"); list.add("gp");
		 * System.out.println("********Action list*******"); System.out.println(list);
		 * sortArraylist(list);
		 */
	}

	private static void sortArraylist(List<String> list) {
		System.out.println("********Iterator*******");
		
		Iterator<String> Itr = list.iterator();
		while(Itr.hasNext()){
			System.out.println(Itr.next());
		}
		System.out.println("*******New foreach********");
		
		list.forEach((arr)-> {
			System.out.println(arr);
		});

		System.out.println("*******Stream********");
		
		list.stream().forEach((arr)->{System.out.println(arr);});
	}
}
