package collection.iteration;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapIteration {

	public static void main(String[] args) {
		Map<Integer, String> students = new HashMap<>();
		students.put(1, "saudagar");
		students.put(3, "amir");
		students.put(4, "anil");
		students.put(2, "amit");
		
		System.out.println("**********Actual map*********");
		System.out.println(students);
		sortMap(students);
		
	}

	private static void sortMap(Map<Integer, String> students) {
		System.out.println("**********While*********");

		for(Map.Entry<Integer, String> entry: students.entrySet()){
			System.out.println(entry.getValue()+":"+entry.getKey());
		}
		
		@SuppressWarnings("unchecked")
		Iterable<Map.Entry<Integer, String>> entry = (Iterable<Entry<Integer, String>>) students.entrySet().iterator();
		
		
		
		students.forEach((key,value)->{
			System.out.println(key+":"+value);
		});
	}
}
