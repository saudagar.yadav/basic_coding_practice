package collection.linkedlist;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedlistTest {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		list.add(5);
		list.add(7);
		list.add(8);
		list.add(18);

		Iterator<Integer> iterator = list.iterator();
		while (iterator.hasNext()) {
			if (iterator.next() == 5) {
				iterator.remove();
			}
		}

		list.forEach(a -> System.out.println(a));
	}
}
