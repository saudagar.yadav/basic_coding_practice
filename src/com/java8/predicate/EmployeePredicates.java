package com.java8.predicate;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EmployeePredicates {

	public static Predicate<Employee> isAdultMale() {
        return p -> p.getAge() > 21 && p.getGender().equalsIgnoreCase("M");
    }
	
	public static List<Employee> filterEmp(List<Employee> employees,
			Predicate<Employee> predicate){
				return employees.stream()
						.filter(predicate)
						.collect(Collectors.<Employee>toList());
	}
}
