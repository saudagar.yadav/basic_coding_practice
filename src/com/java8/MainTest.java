package com.java8;

import java.util.function.Predicate;

public class MainTest {

	public static void main(String[] args) {

		int a[] = {0,10,212,521,52,5,0};
		
		Predicate<Integer> p1=i ->i>10;
		
		for (int j = 0; j < a.length; j++) {
			if(p1.test(a[j])){
				System.out.println();
			}
		}
	
	}
}
