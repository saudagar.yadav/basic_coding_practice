package com.java8.lambda;

public class LambaDemo {
	public static void main(String[] args) {
		
		new Thread(() -> {
			
			for (int i = 0; i < 10; i++) {
				System.out.println("Thread demo : "+i);
			}
		}).start();
	}
}