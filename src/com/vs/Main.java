package com.vs;

public class Main {
	public static void main(String args[]) {

		System.out.println(findDuplicateChar());
	}

	public static char findDuplicateChar() {
		char d = 0;
		String S = "saua";
		boolean flag = true;
		for (int i = 0; i < S.length(); i++) {
			int count = 0;
			if (flag) {
				for (int j = i + 1; j < S.length(); j++) {
					if (S.charAt(i) == S.charAt(j)) {
						count++;
					}
					if (S.length() == j + 1 && count == 0) {
						d = S.charAt(i);
						flag = false;
						break;
					}
				}
			} else {
				break;
			}
		}
		return d;
	}
}
