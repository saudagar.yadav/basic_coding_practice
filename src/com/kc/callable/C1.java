package com.kc.callable;

import java.util.concurrent.Callable;

public class C1 implements Callable<Integer> {

	@Override
	public Integer call() throws Exception {
		for (int i = 0; i < 5; i++) {
			System.out.println("c= " + i);
		}
		return null;
	}

}
