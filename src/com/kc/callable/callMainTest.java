package com.kc.callable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class callMainTest {
	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.submit(new C1());
		executorService.submit(new S1());
	}
}