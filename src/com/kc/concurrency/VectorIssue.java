package com.kc.concurrency;

import java.util.concurrent.CopyOnWriteArrayList;

public class VectorIssue {

	public static void main(String[] args) {
		CopyOnWriteArrayList<Integer> vec = new CopyOnWriteArrayList<Integer>();
		vec.add(56);
		vec.add(25);
		vec.add(55);
		vec.add(52);
		for (Integer integer : vec) {
			System.out.println(integer);
			vec.add(5);
		}
		for (Integer integer : vec) {
			System.out.println(integer);
			vec.add(5);
		}
	}
}
