package com.innerclasses;

//local inner classes
class OuterClass {
	int a = 5;

	public static void innerMethod() {
		class Innerclass {
			void test() {
				System.out.println("Inner class : ");
			}
		}
		Innerclass innerclass = new Innerclass();
		innerclass.test();
	}

	public static void main(String[] args) {
		OuterClass.innerMethod();
	}
}
