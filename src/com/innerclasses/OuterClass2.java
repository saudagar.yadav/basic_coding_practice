package com.innerclasses;

//Static inner classes
class OuterClass2 {
	int a = 5;

	static class Innerclass {
		void test() {
			System.out.println("Inner class : ");
		}
	}

	public static void main(String[] args) {
		OuterClass2.Innerclass in = new OuterClass2.Innerclass();
		in.test();
	}
}
