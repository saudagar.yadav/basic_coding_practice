package com.innerclasses;

public class MainTest {

	public static void main(String[] args) {
		OuterClass3 class3 = new OuterClass3() {
			public void msg() {
				System.out.println("yes");
			}
		};
		class3.msg();
	}
}
