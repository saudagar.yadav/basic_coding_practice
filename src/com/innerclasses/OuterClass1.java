package com.innerclasses;

//Nested inner classes
class OuterClass1 {
	int a = 5;

	class Innerclass {
		void test() {
			System.out.println("Inner class : ");
		}
	}

	public static void main(String[] args) {
		OuterClass1.Innerclass in = new OuterClass1().new Innerclass();
		in.test();
	}
}
