package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

class MysqlCon {
	public static void main(String args[]) {
		try {

			System.out.println("test");

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicedb", "root", "saudu@123");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from bike");
			while (rs.next()) {
				System.out.println(rs.getString("email"));
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}