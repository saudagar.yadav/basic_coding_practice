package com.comparableVsComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainTest {

	public static void main(String[] args) {

		ArrayList<Student> arrayList = new ArrayList<Student>();
		arrayList.add(new Student(1, "saudagar", 5));
		arrayList.add(new Student(2, "amir", 55));
		arrayList.add(new Student(5, "anil", 57));
		arrayList.add(new Student(3, "sachin", 15));

		Collections.sort(arrayList , new SortByAge());

		arrayList.forEach(Student->{
			System.out.println(Student.age);
		});
		
	}
}
