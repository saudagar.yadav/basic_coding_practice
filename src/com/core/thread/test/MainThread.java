package com.core.thread.test;

public class MainThread {
	public static void main(String[] args) throws InterruptedException {
		ITest t1 = new ITest();
		ITest t2 = new ITest();
		
		t1.start();
		t2.start();
		for (int i = 0; i < 3; i++) {
			
			Thread.yield();
			System.out.println(Thread.currentThread().getName());
		}
	}
}
