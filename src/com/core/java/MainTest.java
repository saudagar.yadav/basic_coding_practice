package com.core.java;

import java.util.HashMap;
import java.util.Map;

public class MainTest extends Atest{
	public static void main(String[] args) {
		// testString();
		
		// \u000d System.out.println("test");
				
	}

	private static void testString() {
		String test = "Test string";
		int count = 0;
		char keyTemp;
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		Map<Integer, String> mapNum = new HashMap<Integer, String>();
		for (int i = 0; i < test.length(); i++) {
			keyTemp = Character.toUpperCase(test.charAt(i));
			if (!Character.isWhitespace(keyTemp)) {
				if (map.containsKey(keyTemp)) {
					count = map.get(keyTemp);
					map.put(keyTemp, count + 1);
				} else {
					map.put(keyTemp, 1);
				}
			}
		}
		System.out.println(map);
		map.forEach((key, value) -> {
			if (mapNum.containsKey(value)) {
				mapNum.put(value, mapNum.get(value) + "," + String.valueOf(key));
			} else {
				mapNum.put(value, String.valueOf(key));
			}
		});
		System.out.println(mapNum);

	}

	@Override
	public void test2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void test1() {
		// TODO Auto-generated method stub
		
	}
}
