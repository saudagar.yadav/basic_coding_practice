package com.oracle;

import com.poly.Drawing;
import com.poly.Triangle;

public class Maintest {
	public static void main(String[] args) {
		
		Triangle triangle = new Triangle();
		Drawing drawing = new Drawing();
		drawing.setShape(triangle);
		drawing.draw();
		
	}
}
