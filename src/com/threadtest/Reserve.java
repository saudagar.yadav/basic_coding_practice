package com.threadtest;

public class Reserve implements Runnable {

	int avaiable = 1;
	int wanted;

	Reserve(int i){
		wanted = i;
	}

	@Override
	synchronized public void run() {
		System.out.println("Available : " + avaiable);
		if (avaiable>=wanted) {
			System.out.println(wanted + " Berth reserved for " + Thread.currentThread().getName());
			try {
				Thread.sleep(1500);
				avaiable-=wanted;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("No berth available");
		}
	}
}
