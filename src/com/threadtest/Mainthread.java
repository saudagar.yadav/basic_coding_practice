package com.threadtest;

import com.threadtest.question.EnsureThreadOrder;

public class Mainthread {
	public static void main(String[] args) throws InterruptedException {
		/*Reserve reserve = new Reserve(1);
		Thread t1 = new Thread(reserve,"Saudagar");
		Thread t2 = new Thread(reserve,"Anil");
		
		t1.start();
		t2.start();*/
		/*T1 T1 = new T1();
		Thread t1 = new Thread(T1,"Suresh");
		Thread t2 = new Thread(new T2(),"Ramesh");
		Thread t3 = new Thread(T1,"Kamlesh");
		t1.start();		
		t1.join(1000);
		t2.start();		
		t3.start();*/
		
		//Ensure thread run order
		


		// we have three threads and we need to run in the
		// order T1, T2 and T3 i.e. T1 should start first
		// and T3 should start last.
		// You can enforce this ordering using join() method
		// but join method must be called from run() method
		// because the thread which will execute run() method
		// will wait for thread on which join is called.

		EnsureThreadOrder task1 = new EnsureThreadOrder();
		EnsureThreadOrder task2 = new EnsureThreadOrder();
		EnsureThreadOrder task3 = new EnsureThreadOrder();

		final Thread t1 = new Thread(task1, "Thread - 1");
		final Thread t2 = new Thread(task2, "Thread - 2");
		final Thread t3 = new Thread(task3, "Thread - 3");

		task2.setPredecessor(t1);
		task3.setPredecessor(t2);

		// now, let's start all three threads
		t1.start();
		t2.start();
		t3.start();
	
	}
}
