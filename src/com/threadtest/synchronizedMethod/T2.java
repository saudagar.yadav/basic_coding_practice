package com.threadtest.synchronizedMethod;
public class T2 implements Runnable {
	Table t;

	T2(Table t){
		this.t = t;
	}

	@Override
	public void run() {
		t.printTable(5);
	}

}
