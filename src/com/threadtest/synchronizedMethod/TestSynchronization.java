package com.threadtest.synchronizedMethod;
public class TestSynchronization {
	public static void main(String args[]) {
		Table obj = new Table();// only one object
		T1 t1 = new T1(obj);
		T2 t2 = new T2(obj);
		Thread T1 = new Thread(new T1(obj));
		Thread T2 = new Thread(new T2(obj));
		T1.start();
		T2.start();
	}
}