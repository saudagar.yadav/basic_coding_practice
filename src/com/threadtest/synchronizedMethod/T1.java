package com.threadtest.synchronizedMethod;
public class T1 implements Runnable {
	Table t;

	T1(Table t){
		this.t = t;
	}

	@Override
	public void run() {
		t.printTable(10);
	}

}
